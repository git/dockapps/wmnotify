/*
 * pop3.h
 *
 * Copyright (C) 2003 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This file is released under the GPLv2
 */

#ifndef POP3_H
#define POP3_H 1

#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/* Exported variables */
#undef _SCOPE_
#ifdef POP3_M
#  define _SCOPE_ /**/
#else
#  define _SCOPE_ extern
#endif

#define POP3_ENDL "\r\n" /* CRLF */

#define POP3_CMD_USERNAME "USER"
#define POP3_CMD_PASSWORD "PASS"
#define POP3_CMD_STAT     "STAT"
#define POP3_CMD_QUIT     "QUIT"

#define POP3_RSP_SUCCESS "+OK"
#define POP3_RSP_FAILURE "-ERR"

int POP3_CheckForNewMail(void);

#endif /* POP3_H */
