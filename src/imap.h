/*
 * imap.h
 *
 * Copyright (C) 2003 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This file is released under the GPLv2
 */

#ifndef IMAP_H
#define IMAP_H 1

/* Exported variables */
#undef _SCOPE_
#ifdef IMAP_M
#  define _SCOPE_ /**/
#else
#  define _SCOPE_ extern
#endif

int IMAP4_CheckForNewMail(void);

#endif				/* IMAP_H */
