/*
 * network.h
 *
 * Copyright (C) 2003 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This file is released under the GPLv2
 */

#ifndef NETWORK_H
#define NETWORK_H 1

#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/* POP3 responses may be up to 512 characters long, including the terminating
   CRLF. IMAP4 responses can be more than 1024 characters. */
#define WMNOTIFY_BUFSIZE 10240

int SocketOpen(char *server_name, int port);

int ConnectionEstablish(char *server_name, int port);

int ConnectionTerminate(void);

int WmnotifySendData(char *buffer, int size);

int WmnotifyGetResponse(char *buffer, int max_size);

#endif				/* NETWORK_H */
