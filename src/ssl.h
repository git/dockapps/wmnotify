/*
 * ssl.h
 *
 * Copyright (C) 2003 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This file is released under the GPLv2
 */

#ifndef SSL_H
#define SSL_H 1

#if HAVE_CONFIG_H
#  include "config.h"
#endif

#if HAVE_SSL

#include <openssl/ssl.h>
#include <openssl/err.h>

/* Exported variables */
#undef _SCOPE_
#ifdef SSL_M
#  define _SCOPE_ /**/
#else
#  define _SCOPE_ extern
#endif

#define FAIL    -1

struct ssl_infos_t {
	SSL_CTX *ctx;
	SSL *ssl;
};

_SCOPE_ struct ssl_infos_t ssl_infos;

SSL_CTX *InitCTX(void);

void ShowCerts(SSL *ssl);

int InitSSL(int sock_fd);

#endif /* HAVE_SSL */

#endif /* SSL_H */
