/*
 * xevents.h
 *
 * Copyright (C) 2003 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This file is released under the GPLv2
 */

#ifndef XEVENTS_H
#define XEVENTS_H 1

void AudibleBeep(void);

void
ProcessXlibEventsInit(void (*single_click_callback) (void),
		      void (*double_click_callback) (void));

void ProcessXlibEvents(void);

#endif				/* XEVENTS_H */
