/*
 * dockapp.h
 *
 * Copyright (C) 2003 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This file is released under the GPLv2
 */

#ifndef DOCKAPP_H
#define DOCKAPP_H 1

#include <X11/xpm.h>

struct XpmIcon {
	XpmAttributes attributes;
	Pixmap shapemask;
	Pixmap image;
};

struct dockapp_t {
	Display *display;
	Window root_win;
	Window win;
	Window iconwin;
	int screen;
	int d_depth;
	Pixel back_pix;
	Pixel fore_pix;
	GC NormalGC;
	struct XpmIcon xpm_icon;
};

void
InitDockAppWindow(int argc, char *argv[], char *pixmap_data[]);

void RedrawWindow(void);

void
copyXPMArea(int x, int y, unsigned int sx, unsigned int sy, int dx,
	    int dy);

/* Exported variables */
#undef _SCOPE_
#ifdef DOCKAPP_M
#define _SCOPE_ /**/
#else
#define _SCOPE_ extern
#endif

_SCOPE_ struct dockapp_t dockapp;

#endif				/* DOCKAPP_H */
