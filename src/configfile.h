/*
 * configfile.h
 *
 * Copyright (C) 2003 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This file is released under the GPLv2
 */

#ifndef CONFIGFILE_H
#define CONFIGFILE_H 1

void ConfigurationFileInit(void);

#endif /* CONFIGFILE_H */
