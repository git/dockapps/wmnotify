/*
 * sound.h
 *
 * Copyright (C) 2003 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This file is released under the GPLv2
 */

#ifndef SOUND_H
#define SOUND_H 1

void PlayAudioFile(char *filename, int volume);

#endif				/* SOUND_H */
